To define a user space in Imagine, create a YAML file in this directory using the pattern:

    username.yml

The filename of this file determines your 'user' e.g: `joe.yml` will give you the path: `<<imagine>>/joe/`

Each key in this yaml file defines a 'project' and the value of the key defines the source url to be scraped for images.
e.g.:

    images: http://www.brianmcmurray.com/images/

would give you the path: `<<imagine>>/joe/images/` and the image path  specified in the request would be appended to `http://www.brianmcmurray.com/images/` when being requested by imagine.