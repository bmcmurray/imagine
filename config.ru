require 'fileutils'
require 'erb'
require 'yaml'

require 'rubygems'
require 'bundler/setup'
require 'sinatra'
require 'typhoeus'
require 'rmagick'
require 'rack/protection'
require 'time'

require './app/imagine/image'
require './app/imagine/imagine'
require './app/models/project'
require './app/models/user'

use Rack::Protection, :except => [:session_hijacking, :remote_token]

run Rack::URLMap.new("/" => Imagine::Imagine)