require 'sinatra/base'
require 'erb'

module Imagine

  class Imagine < Sinatra::Base
    set :sessions, false

    configure :production, :development do
      enable :logging
    end

    set :views, './app/imagine/views'

    helpers do
      def user
        @user ||= User.find(params[:user]) || halt(404)
      end

      def project
        @project ||= user.projects.fetch(params[:project])
      rescue KeyError
        halt 404
      end
    end

    get %r{(/.*)[/]$} do
      redirect "#{params[:captures].first}"
    end

    get '/' do
      erb :index
    end

    get '/:user/:project/:filter/*/*' do
      path_fragments = []

      for path in params[:splat][0..-1] do
        path_fragments.push(ERB::Util.url_encode(path))
      end

      file = path_fragments.join('/')

      img_destination = "#{project.img_directory}/#{params[:filter]}/#{file}"

      remote_img_destination = "#{project.src}#{file}"

      send_file Image.filter(remote_img_destination, params[:filter], img_destination)

    end

    not_found do
      "404 - Not Found"
    end

  end
end