require 'time'

module Imagine
  class Image
    def self.filter(remote_img_destination, filter, img_destination)
      # Validate the filter

      # Verify sizes are valid
      sizes = filter.match(/([1-9]\d*)x([1-9]\d*)/)
      halt 500 unless sizes
      width = sizes[1].to_i
      height = sizes[2].to_i

      # Should check the headers of file about to be fetched for max file size we want to allow
      # and that it returns a valid image mimetype BEFORE we download the file for manipulation

      # Perhaps for major deployment it makes sense to job these out to a queue that lets image
      # fetching happen in a separated, secure environment that also scrubs for viruses with Suhosin

      # Should we store the generated image on S3 or some other file store?

      # Do we want to set up some sort of request cache and/or log to be able to lock if too many
      # requests? We want to keep in mind this could be spammed as a way to DDOS a server.

      # Fetch image info
      response = Typhoeus::Request.head(remote_img_destination, :followlocation => true)

      # Check the response, return a 404 if we can't find it or encounter some other error
      halt 404 unless response.success?

      # # Check to see if we already have this derivative
      if File.exist?(img_destination)
        # Check to see if our derivative is out of date
        if File.mtime(img_destination) > Time.parse(response.headers_hash["Last-Modified"])
          return img_destination
        end
      end

      # Reject images if they are larger than 2mb
      halt 413 unless response.headers_hash["Content-Length"].to_i < 2000000

      # Reject items that don't identify themselves as images
      halt 415 unless response.headers_hash["Content-Type"].start_with? 'image'

      # Make sure directories will exist
      FileUtils.mkdir_p(File.dirname(img_destination))

      # Fetch an image directly
      image_response = Typhoeus::Request.get(remote_img_destination, :followlocation => true)

      image_data = image_response.body
      image = Magick::Image::from_blob(image_data).first
      image.resize_to_fit!(width, height)
      image.write(img_destination)

      return img_destination
    end
  end
end