require 'yaml'

require './app/models/project'

class User
  @@base_directory = Dir.pwd

  def User.find(user_id)
    user_path = "#{@@base_directory}/config/users/#{user_id}.yml"
    if File.exists?(user_path)
      User.new(user_id, user_path)
    end
  end

  def initialize(username, file_path)
    @username   = username

    # Create user directory
    user_directory_path = "#{@@base_directory}/data/images/#{username}"
    Dir.mkdir(user_directory_path) unless File.exists?(user_directory_path)
    
    # Load projects
    projects = YAML.load_file(file_path)
    @projects = {}
    projects.each {|project_id, src_url| @projects[project_id]= Project.new(project_id, username, src_url)}
  end

  def username
    @username
  end

  def projects
    @projects
  end

end