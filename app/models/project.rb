class Project
  @@base_directory = Dir.pwd

  def initialize(project_name, username, src_url)
    @project_name = project_name
    @src_url = src_url

    # Make sure project has image directory
    img_directory_path = "#{@@base_directory}/data/images/#{username}/#{project_name}"
    Dir.mkdir(img_directory_path) unless File.exists?(img_directory_path)
    @img_dir = img_directory_path
  end

  def project_name
    @project_name
  end

  def src
    @src_url
  end

  def img_directory
    @img_dir
  end

end