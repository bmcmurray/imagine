# Imagine -- simple image derivative hosting
© 2013-2015 Brian McMurray

## Install

1. First, install Ruby on your system and Bundler.

    Run `bundle install`

2. Install ImageMagick.

    Run `brew install imagemagick`

## Run locally in development

Run `rackup config.ru` from the project root.